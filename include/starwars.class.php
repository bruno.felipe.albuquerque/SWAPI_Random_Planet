<?php
	require_once 'config.ini.php';
	
	class StarWarsPlanets{
		
		public static $qty_planets = 0;
		
		function __construct() {
			$this->qty_planets = $this->checkNumberOfPlanets();
		}
		
		private function checkNumberOfPlanets() {
			return $this->getCURL(API_ADDRESS_PLANETS, "count");
		}
		
		public function getRandomPlanet() {
			$planet_url = API_ADDRESS_PLANETS . random_int(1, $this->qty_planets); //Requires PHP 7
			
			return $this->getCURL($planet_url);
		}
		
		private function getCURL($request_url, $request_value = FALSE) {
			(function_exists('curl_init')) ? '' : die('cURL must be intalled, or else "getCURL" won\'t work.');

			$res	= array();
			$curl	= curl_init();
		
			curl_setopt_array($curl, array(
					CURLOPT_URL => $request_url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_FOLLOWLOCATION => true, //Avoid HTTP 301
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_HTTPHEADER => array(
							"cache-control: no-cache",
							"content-type: application/x-www-form-urlencoded"),
			));
			
			$res['content'] 	= curl_exec($curl);
			$res['error'] 		= curl_error($curl);
			$res['http_code'] 	= curl_getinfo($curl, CURLINFO_HTTP_CODE);
			$content 			= json_decode($res['content'], true);
			
			curl_close($curl);
			
			if ($res['http_code'] === 200) {
				return (!$request_value ? $content : $content[$request_value]);
			} else {
				die("getCURL Error :: HTTP {$res['http_code']}. {$res['error']}");
			}
		}
		
		public function formatPlanetData($data) {
			if(is_numeric($data)) {
				return number_format($data, 0, '.', ',');
			} else {
				return $data;				
			}
		}
	}
?>