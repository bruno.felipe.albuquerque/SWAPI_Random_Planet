<?php
	declare(strict_types = 1);
	
	define('API_ADDRESS', 'https://swapi.co/api');
	define('API_ADDRESS_PLANETS', API_ADDRESS.'/planets/');
?>