<?php
	session_start();
	require_once 'include/starwars.class.php';
	
	$starwars_planets = (object) array();
	
	if(isset($_SESSION["StarWarsPlanet"])) {
		$starwars_planets = unserialize($_SESSION["StarWarsPlanet"]);
	} else {
		$starwars_planets = new StarWarsPlanets();
		$_SESSION["StarWarsPlanet"] = serialize($starwars_planets);
	}
	
	$planet_data = $starwars_planets->getRandomPlanet();
	
?>
<!DOCTYPE html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="robots" content="noindex">
		<title>Star Wars Plane Trivia - Powered by SWAPI</title>
		
		<link rel="shortcut icon" href="images/galactic_empire.png">
		<link href="https://fonts.googleapis.com/css?family=Poor+Story" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>
	<body>
		<div class="content">
			<div class="info">
				<h1><?=$planet_data["name"]?></h1>
				<p><b>Rotation (In Hours):</b> <?php echo $starwars_planets->formatPlanetData($planet_data["rotation_period"]); ?></p>
				<p><b>Orbital Period (In Days):</b> <?php echo $starwars_planets->formatPlanetData($planet_data["orbital_period"]); ?></p>
				<p><b>Diameter (In Km):</b> <?php echo $starwars_planets->formatPlanetData($planet_data["diameter"]); ?></p>
				<p><b>Climate:</b> <?=$planet_data["climate"]?></p>
				<p><b>Gravity (In Earth Units):</b> <?=$planet_data["gravity"]?></p>
				<p><b>Terrain:</b> <?=$planet_data["terrain"]?></p>
				<p><b>Surface Water:</b> <?=$planet_data["surface_water"]?></p>
				<p><b>Population:</b> <?php echo $starwars_planets->formatPlanetData($planet_data["population"]); ?></p>
				<p>
					<i>
					<?php
						$film_count = count($planet_data["films"]);
						if($film_count > 0) {
							$plural = ($film_count == 1 ? "" : "s");
							echo "Featured in $film_count film$plural.";
						} else {
							echo "Not featured in any film.";
						}
					?>
					</i>
				</p>
			</div>
			<p class="button"><button type="button" class="refresh" onClick="window.location.reload()">Next Planet</button></p>
		</div>
		
	</body>
</html>